using DelimitedFiles

dat = reshape(DelimitedFiles.readdlm("input01.txt", Int), :)
@info "Part 1 -- number of sequential increases: $(count(>(0), diff(dat)))"


function sliding_window_sum(v, n)
    return [sum(view(v, i:(i+n-1))) for i in 1:(length(v)-n+1)]
end

@info "Part 2 -- number of sequential width-3 window increases: $(count(>(0), diff(sliding_window_sum(dat,3))))"

nothing
