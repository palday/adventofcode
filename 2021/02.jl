using DelimitedFiles

dat = reshape(DelimitedFiles.readdlm("input02.txt", String), :, 2)

function position_delta(command)
    delta = parse(Int, command[2])
    direction = command[1]

    if direction == "forward"
        return [delta, 0]
    elseif direction == "up"
        return [0, -delta]
    elseif direction == "down"
        return [0, delta]
    else
        throw(ArgumentError("Invalid direction: $direction"))
    end
end

@info "Part 1 -- product of final position: $(*(mapreduce(position_delta, +, eachrow(dat))...))"

function update_position(current, next)
    movement, aim_change = next
    position, depth, aim = current
    # there's no conditional here because movement xor aim_change is zero
    return [movement + position , movement * aim + depth, aim + aim_change]
end

@info "Part 2 -- product of final position: $(cumprod(mapfoldl(position_delta, update_position, eachrow(dat); init=[0, 0, 0]))[2])"

nothing
