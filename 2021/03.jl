using Test

function load(io)
    val = mapreduce(hcat, readlines(io)) do line
        return parse.(Bool, collect(line))
    end
    BitMatrix(val')
end

function int(b::AbstractArray{Bool, 1})
    pows = collect(length(b):-1:1) .- 1
    vv = sum(zip(b, pows)) do (bit, p)
        return bit * (2^p)
    end
    return Int(vv)
end

testdat = """
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
"""
function mode(x::AbstractArray{Bool, 1}, ties=true)
    s = 2 * sum(x)
    l = length(x)
    return  s == l ? ties : s > l
end

function gamma(m::BitMatrix)
    return int(BitArray(mode(col) for col in eachcol(m)))
end


epsilon(m::BitMatrix) = gamma(.!m)

tdat = load(IOBuffer(testdat))
@test gamma(tdat) == 22
@test epsilon(tdat) == 9

dat = load("input03.txt")

@info "Part 1 -- product of gamma and epsilon: $(gamma(dat) * epsilon(dat))"

function filter_by_colmode(m::BitMatrix, col::Int, invert=false, ties=false)
    modus = mode(view(m, :, col), ties)
    filt = [el[col] == modus for el in eachrow(m)]
    if invert
        filt = .!filt
    end
    return m[filt, :]
end

function gasval(m, invert, ties)
    for i in 1:size(m,2)
        m = filter_by_colmode(m, i, invert, ties)
        size(m, 1) == 1 && break
    end
    return int(only(eachrow(m)))
end

oxval(m) = gasval(m, false, true)
@test oxval(tdat) == 23
coval(m) = gasval(m, true, true)
@test coval(tdat) == 10

@info "Part 1 -- product of oxygen generator and CO2 scrubber: $(oxval(dat) * coval(dat))"
