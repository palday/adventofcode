using DelimitedFiles

n, l, p = first(eachrow(DelimitedFiles.readdlm("input")))

function solution1()
    sum(eachrow(DelimitedFiles.readdlm("input"))) do (n, l, p)
        lb, ub = parse.(Int, (split(n,"-")))
        lb <= count(chop(l), p) <= ub
    end
end

@show solution1()

function solution2()
    sum(eachrow(DelimitedFiles.readdlm("input"))) do (n, l, p)
        lb, ub = parse.(Int, (split(n,"-")))
        count(chop(l), p[[lb,ub]]) == 1
    end
end

@show solution2()
