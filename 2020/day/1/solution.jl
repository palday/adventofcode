using Combinatorics
using DelimitedFiles

dat = reshape(DelimitedFiles.readdlm("input", Int), :)

function solution(i)
  for pair in combinations(dat,i)
       sum(pair) == 2020 && return prod(pair)
  end
end

@show solution(2)
@show solution(3)

nothing
